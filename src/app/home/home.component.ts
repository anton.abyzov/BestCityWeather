import { Component, OnInit, OnChanges } from '@angular/core';
import { finalize } from 'rxjs/operators';
import { WeatherService } from '@app/services/weather.service';
import { CityInfo } from '@app/models/cityInfo';
import { sortBy } from 'underscore';
import { BESTFORWOMEN, BESTFORMEN, BESTHUMIDITY } from '@app/models/constans';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  public cityInfos: CityInfo[] = null;
  isLoading: boolean;

  public columnsToDisplay = ['name', 'temperature', 'humidity', 'rating'];

  public choosedMale = 'Male';
  public lastMale: string = null;
  public maleChoose = [
      'Male',
      'Female',
    ];

  constructor(private weatherService: WeatherService) { }

  ngOnInit() {
    this.isLoading = true;
    this.weatherService.getCityInfos().subscribe(infos => {

        this.isLoading = false;

        this.prepareAndSortCityByRating(infos);

        this.cityInfos = infos.reverse();
    });
  }

  prepareAndSortCityByRating(cityInfos: CityInfo[], isForWomen: boolean = false) {
    let bestTemp: number = BESTFORMEN;

    if (isForWomen) {
        bestTemp = BESTFORWOMEN;
    }

    cityInfos.map(i => {
        const tempRating = Math.abs(bestTemp - i.temperature);
        const humidityRating = Math.abs(BESTHUMIDITY - i.humidity);
        i.rating = (tempRating + humidityRating) / 2;
    });

    this.cityInfos = cityInfos.sort((a, b) => b.rating - a.rating);
  }

  reCalculateForMale() {
      if (this.choosedMale === 'Male') {
        this.prepareAndSortCityByRating(this.cityInfos, false);
      }
      if (this.choosedMale === 'Female') {
          this.prepareAndSortCityByRating(this.cityInfos, true);
      }
  }
}
