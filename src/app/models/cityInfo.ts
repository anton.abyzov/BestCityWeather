export class CityInfo {
    id: number;
    name: string;
    temperature: number;
    humidity: number;

    rating: number;

    constructor(id: number, name: string, temperature: number, humidity: number) {
        this.id = id;
        this.name = name;
        this.temperature = Math.round(temperature * 100) / 100;
        this.humidity = humidity;
    }
}
