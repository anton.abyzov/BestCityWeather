import Location from '../models/location';

export class GeoService {
    public currentLocation: Location;

    public getPosition(): Location {
      navigator.geolocation.getCurrentPosition((position) => {
        this.currentLocation = new Location(position.coords.latitude, position.coords.longitude);
      });

      return this.currentLocation;
    }
}
