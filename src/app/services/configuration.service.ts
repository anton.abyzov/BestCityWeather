import * as settings from '../../environments/settings';

export class ConfigurationService {

    getValueByKey(key: string): string {
        return settings.default[key];
    }

    getApiKey(): string {
        return this.getValueByKey('apiKey');
    }

    getApiAddress(): string {
        return this.getValueByKey('apiAddress');
    }
}
