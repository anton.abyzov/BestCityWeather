import { ConfigurationService } from '@app/services/configuration.service';
import Location from '../models/location';
import { CityInfo } from '@app/models/cityInfo';
import axios from 'axios';
import { GeoService } from '@app/services/geo.service';


import { Observable } from 'rxjs/Observable';
import {HttpClient} from '@angular/common/http';
import 'rxjs/add/operator/map';

import { KELVIN } from '@app/models/constans';

export class WeatherService {

    private location: Location;

    constructor(
        private configService: ConfigurationService,
        private geo: GeoService,
        private client: HttpClient,
    ) { }

    getCityInfos(cityCount: number = 50, location: Location = null): Observable<CityInfo[]> {

        if (location == null) {
            setTimeout(() => { this.location = this.geo.getPosition(); }, 2000);
        } else {
            this.location = location;
        }

        if (location == null) {
            this.location = new Location(51.509865, -0.118092); // London location
        }

        const appid = this.configService.getApiKey();
        const url = `${this.configService.getApiAddress()}lat=${this.location.lat}&lon=${this.location.lon}` +
            `&cnt=${cityCount}&appid=${appid}`;

        return this.client.get(url).map((data: any) => {
            return data.list.map(this.mapDataToCityInfosFunction);
        });
    }

    private mapDataToCityInfosFunction = (c: any) => {
            return new CityInfo(c.id, c.name, c.main.temp - KELVIN, c.main.humidity);
    }
}
